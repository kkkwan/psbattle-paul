All code and files for PSBattle: Paul.
Features include:

 - Import a background from your phone's gallery
 - Import a background from a URL
 - Edit Paul
    - Flip Paul on tap
    - Drag Paul around the canvas
    - Rotate Paul on drag and hold
    - Resize Paul on drag and hold
   
 - Save Image
 - Share Image (which saves the image first)
    - Choose an application to share your image
       - Upload it to your Dropbox
	   - Text it to a friend
	   - etc.

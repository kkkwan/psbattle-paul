package com.example.kkkwan.psbattlepaul;

/**
 * Created by Kelly on 3/27/2018.
 *
 * Example class to store information for my predefined examples.
 */

public class Example {
    private int id;
    private String name;
    private float[] xy;
    private int direction;
    private float rotation;
    private float resize;
    private String fileName;

    public Example(){

    }

    public Example (int id, String name, float[] xy, int direction, float rotation, float resize, String fileName){
        this.id = id;
        this.name = name;
        this.xy = xy;
        this.direction = direction;
        this.rotation = rotation;
        this.resize = resize;
        this.fileName = fileName;
    }

    public void setID(int id){
        this.id = id;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setXY(float[] xy){
        this.xy = xy;
    }

    public void setDirection(int direction){
        this.direction = direction;
    }

    public void setRotation(float rotation){
        this.rotation = rotation;
    }

    public void setResize(float resize){
        this.resize = resize;
    }

    public void setFileName(String fileName){
        this.fileName = fileName;
    }

    public int getID(){
        return id;
    }

    public String getName(){
        return name;
    }

    public float[] getXY(){
        return xy;
    }

    public int getDirection(){
        return direction;
    }

    public float getRotation(){
        return rotation;
    }

    public float getResize(){
        return resize;
    }

    public String getFileName(){
        return fileName;
    }


}

package com.example.kkkwan.psbattlepaul;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Kelly on 3/23/2018.
 */

public class CanvasView extends View {

    public CanvasView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initCanvas();
    }

    protected enum ToolOption {FLIP, DRAG, ROTATE, RESIZE}

//    final float HEIGHT_WIDTH_RATIO = 1066f/486; //height of original paul image / width
    private ToolOption toolSelected;
    private float[] center;
    private float touchX, touchY;
    private float centerX, centerY;
    private float offsetX, offsetY;
    private float dragX, dragY;
    private float resizeX, resizeY;
    private float rotateX, rotateY;
    private float ratio;
    private float rotation;
    private Canvas canvas;
    private Bitmap canvasBitmap;
    private Drawable[] layers = new Drawable[2];
    private Drawable transparentDrawable;
    private Drawable whiteDrawable;
    private LayerDrawable layerDrawable;
    private Drawable paulDrawable;
    private Drawable keanuDrawable;
    private Drawable lunchDrawable;
    private Drawable eagleDrawable;
    private Drawable background;
    private Paint paint = new Paint();
    InputStream paulStream;
    InputStream keanuStream;
    InputStream lunchStream;
    InputStream eagleStream;
    Bitmap ogPaulBitmap;
    Bitmap paulBitmap;
    Bitmap transparentBitmap;
    Bitmap paulLayerBitmap;
    int height;
    int width;
    int paulHeight;
    int paulWidth;
    int direction; //0 for left, 1 for right
    int exampleIndex; //0, 1, 2
    Map<String, Example> examplesMap = new HashMap<String, Example>();

    private void initCanvas() {
        toolSelected = ToolOption.DRAG;
        touchX = touchY = -1;
        centerX = centerY = -1;
        offsetX = offsetY = -1;
        dragX = dragY = -1;
        resizeX = resizeY = -1;
        rotateX = rotateY = -1;
        height = width = -1;
        paulWidth = paulHeight = -1;
        ratio = 1;
        rotation = 0;

        transparentDrawable = new ColorDrawable(Color.TRANSPARENT);
        whiteDrawable = new ColorDrawable(Color.WHITE);
        paulDrawable = transparentDrawable;

        try {
            //paul image put in a square for easier rotation
            paulStream = getResources().getAssets().open("paulAlpha_square.png");
            //edited because I erased some floating bits that kept making me think my screen was dusty
//            paulStream = getResources().getAssets().open("paulAlpha_edited.png");

            //three example backgrounds
            keanuStream = getResources().getAssets().open("keanu.jpg");
            lunchStream = getResources().getAssets().open("lunch-atop-skyscraper.jpg");
            eagleStream = getResources().getAssets().open("eagle.jpg");

        } catch (Exception e) {
            e.printStackTrace();
        }

        //largest index on top
        layers[0] = transparentDrawable; //will be initial white background
        layers[1] = transparentDrawable; //will be background layer
        layerDrawable = new LayerDrawable(layers);

        layerDrawable.setId(0, 0);
        layerDrawable.setId(1, 1);

        background = layerDrawable;

        //set predefined examples
        //id, name, offset, direction, rotation, ratio
        examplesMap.put("Keanu", new Example(0, "Keanu", new float[]{616-290, 65}, 1, 0, 1, "keanu.jpg"));
        examplesMap.put("Lunch", new Example(1, "Lunch", new float[]{752-180, 512}, 0, 0, 0.588f, "lunch-atop-skyscraper.jpg"));
        examplesMap.put("Eagle", new Example(2, "Eagle", new float[]{362-80, 502}, 1, 0, 0.350f, "eagle.jpg"));
        direction = 1; //paul image originally looking to the right
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        try {
            transparentBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);

            //backgrounds will be drawables to change easier
            paulDrawable = Drawable.createFromStream(paulStream, null);
            keanuDrawable = Drawable.createFromStream(keanuStream, null);
            lunchDrawable = Drawable.createFromStream(lunchStream, null);
            eagleDrawable = Drawable.createFromStream(eagleStream, null);
            keanuStream.close();
            lunchStream.close();
            eagleStream.close();

            //paul will be drawn on top of the background
            ogPaulBitmap = drawableToBitmap(paulDrawable);
            paulBitmap = ogPaulBitmap.copy(Bitmap.Config.ARGB_8888, true);

            //get dimensions when the view is drawn
            paulWidth = paulBitmap.getWidth();
            paulHeight = paulBitmap.getHeight();

            paulLayerBitmap = mergeImages(transparentBitmap, paulBitmap, 0, 0);

            center = getCenter(paulBitmap);
            centerX = center[0];
            centerY = center[1];

            canvasBitmap = paulLayerBitmap.copy(Bitmap.Config.ARGB_8888, true);
            canvas = new Canvas(canvasBitmap);

            height = this.getHeight();
            width = this.getWidth();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDraw(Canvas canvas) {
        if(canvas != null) {
            super.onDraw(canvas);
            canvas.drawBitmap(canvasBitmap, 0, 0, paint);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        //Each touch flips Paul horizontally (doesn't flip until you lift finger)
        if (getTool() == ToolOption.FLIP) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                case MotionEvent.ACTION_MOVE:
                    break;
                case MotionEvent.ACTION_UP:
                    if (direction == 1) {
                        direction = 0;
                    } else if (direction == 0) {
                        direction = 1;
                    }
                    drawPaul();
                    break;
                default:
                    return false;
            }
        //Touch, hold, and drag to drag Paul
        } else if (getTool() == ToolOption.DRAG) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touchX = event.getX();
                    touchY = event.getY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    float tempOffsetX = 0;
                    float tempOffsetY = 0;

                    dragX = event.getX() - touchX;
                    dragY = event.getY() - touchY;
                    touchX = event.getX();
                    touchY = event.getY();

                    tempOffsetX = offsetX + dragX;
                    tempOffsetY = offsetY + dragY;

                    //don't allow paul to be moved off the screen (:
                    //if he is, move him back into the bounds
                    if (tempOffsetX > 0 - centerX && tempOffsetX < width - centerX) {
                        offsetX += dragX;
                    } else if (tempOffsetX <= 0 - centerX){
                        offsetX = 0 - centerX + 1;
                    } else if (tempOffsetX >= width - centerX){
                        offsetX = width - centerX - 1;
                    }
                    if (tempOffsetY > 0 - centerY && tempOffsetY < height - centerY) {
                        offsetY += dragY;
                    } else if (tempOffsetY <= 0 - centerY) {
                        offsetY = 0 - centerY + 1;
                    } else if (tempOffsetY >= height - centerY){
                        offsetY = height - centerY - 1;
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    break;
                default:
                    return false;

            }
        //Rotate Paul on touch and hold
        } else if (getTool() == ToolOption.ROTATE) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touchX = event.getX();
                    touchY = event.getY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    rotateX = event.getX() - touchX;
                    rotateY = event.getY() - touchY;

                    float tempRotation;
                    //an arbitrary value that isn't too large for smoother rotation
                    //uses touch and drag points to determine which way to rotate
                    //basically the speed and direction to rotate
                    tempRotation = (float)((rotateX+rotateY)/2/10);
                    rotation += tempRotation;

                    drawPaul();

                    if(rotation <= -360 || rotation >= 360){
                        rotation = 0;
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    break;
                default:
                    return false;
            }
        //Resize Paul on touch and hold
        } else if (getTool() == ToolOption.RESIZE) {
            int tempWidth = 0;
            int tempHeight = 0;
            float tempRatio = 0;

            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    touchX = event.getX();
                    touchY = event.getY();
                    break;
                case MotionEvent.ACTION_MOVE:
                    resizeX = event.getX() - touchX;
                    resizeY = event.getY() - touchY;

                    //ratio for how much bigger or smaller image should be compared to previous size
                    //get the amount dragged horizontally and compare with width for ratio
                    //get same ratio for height, and average the two
                    //basically the speed of the size change
                    tempRatio = ((resizeX + width) / width + (resizeY + height) / height)/2f;

                    tempWidth = (int) (paulWidth * tempRatio);
                    tempHeight = tempWidth;
//                    tempHeight = (int) (tempWidth * HEIGHT_WIDTH_RATIO); //keep aspect ratio

                    //allow paul to be a reasonable size (:
                    if (tempWidth > 50 && tempWidth < width * 2 && tempHeight > 50 && tempHeight < height * 2) {
                        paulWidth = tempWidth;
                        paulHeight = tempHeight;

                        //remember the overall ratio
                        ratio = paulWidth/(float)ogPaulBitmap.getWidth();

                        drawPaul();

                        centerX = getCenter(paulBitmap)[0];
                        centerY = getCenter(paulBitmap)[1];
                    }

                    break;
                case MotionEvent.ACTION_UP:
                    break;
                default:
                    return false;

            }
        }

        showPaul();
        return true;
    }

    /**
     * Sets the background image from Gallery intent
     *
     * @param context
     * @param r
     * @param data
     */
    public void setImageFromGallery(Context context, Resources r, Intent data) {
        layerDrawable.setDrawableByLayerId(0, whiteDrawable);

        Drawable bgImgDrawable;

        try {
            Uri selectedImage = data.getData(); //path for selected image (not real path)

            //Get the image from the gallery

            //These two lines will work for most cases, but
            //Uses too much memory for extremely large images...
//                InputStream inputStream = getContentResolver().openInputStream(selectedImage);
//                bgImgDrawable = Drawable.createFromStream(inputStream, selectedImage.toString());

            //used method found online to compress images,
            // which needed absolute file path instead of uri
            String filePath = ExternalMethods.getRealPathFromUri(context, Uri.parse(selectedImage.toString()));
            Bitmap bgBitmap = ExternalMethods.decodeFile(new File(filePath));

            //resize the background to fit canvas
            bgBitmap = Bitmap.createScaledBitmap(bgBitmap, this.getWidth(), this.getHeight(), false);

            //set background to middle layer
            bgImgDrawable = bitmapToDrawable(bgBitmap);
            layerDrawable.setDrawableByLayerId(1, bgImgDrawable);

        } catch (Exception e) {
            e.printStackTrace();
        }

        //set the new background
        background = layerDrawable;
        setBackground(background);
    }

    /**
     * Attempts to load an image from a url String.
     * @param url
     */
    public void setImageFromURL(String url){

        //Using network must be done asynchronously
        DownloadImageHelper dlImg = new DownloadImageHelper() {
            Drawable bgDrawable;

            @Override
            protected void onPostExecute(Bitmap bitmapRetrieved) {
                super.onPostExecute(bitmapRetrieved);

                //if the image could not be loaded, show a reason
                if(bitmapRetrieved == null) {
                    if (getRetrievedImgBool() == 1) {
                        Toast.makeText(getContext(), "Could not retrieve image; check internet connection.", Toast.LENGTH_LONG).show();
                    } else if (getRetrievedImgBool() == 2) {
                        Toast.makeText(getContext(), "Invalid URL", Toast.LENGTH_LONG).show();
                    } else if (getRetrievedImgBool() == 3) {
                        Toast.makeText(getContext(), "Image could not be found", Toast.LENGTH_LONG).show();
                    } else if (getRetrievedImgBool() == 4) {
                        Toast.makeText(getContext(), "Could not retrieve image", Toast.LENGTH_LONG).show();
                    }  else if (getRetrievedImgBool() == 5) {
                        Toast.makeText(getContext(), "Image too large to load", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getContext(), "URL was not a valid image location", Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    //if the image could be loaded, set it as the background
                    bgDrawable = bitmapToDrawable(bitmapRetrieved);

                    //resize the background to fit the canvas, especially in case the image is huge or out of memory can occur
                    bgDrawable = bitmapToDrawable(Bitmap.createScaledBitmap(drawableToBitmap(bgDrawable), width, height, false));

                    layerDrawable.setDrawableByLayerId(1, bgDrawable);
                    background = layerDrawable;
                    setBackground(background);
                    invalidate();
                }
            }
        };

        dlImg.execute(url);
    }

    /**
     * Display predefined examples.
     */
    public void showExample() {
        String key = "";

        switch (exampleIndex) {
            case 0:
                key = "Keanu";
                layerDrawable.setDrawableByLayerId(1, keanuDrawable);
                break;
            case 1:
                key = "Lunch";
                layerDrawable.setDrawableByLayerId(1, lunchDrawable);
                break;
            case 2:
                key = "Eagle";
                layerDrawable.setDrawableByLayerId(1, eagleDrawable);
                break;
            default:
                break;

        }

        //get all the necessary information
        direction = examplesMap.get(key).getDirection();
        offsetX = examplesMap.get(key).getXY()[0];
        offsetY = examplesMap.get(key).getXY()[1];
        ratio = examplesMap.get(key).getResize();
        rotation = examplesMap.get(key).getRotation();

        //apply the changes to the original paul image, show the background, show paul
        drawPaul();
        setBackground(layerDrawable);
        showPaul();

        //I only have 3 examples; after the third example, cycle back to the first one
        if (++exampleIndex > 2) {
            exampleIndex = 0;
        }
    }

    /**
     * Transparent areas are saved black.
     * This sets the lowest layer to white so there should be no transparent areas.
     */
    public void setTransparentToWhite() {
        layerDrawable.setDrawableByLayerId(0, whiteDrawable);
        background = layerDrawable;
        setBackground(background);
    }

    /**
     * Sets which tool is currently being used
     *
     * @param toolSelected
     */
    public void setTool(ToolOption toolSelected) {
        this.toolSelected = toolSelected;
    }

    /**
     * Get which tool is selected
     * @return ToolOption
     */
    public ToolOption getTool() {
        return toolSelected;
    }

    /**
     * Flips the bitmap horizontally.
     *
     * @param bitmap
     * @return Bitmap
     */
    public Bitmap flipHorizontally(Bitmap bitmap) {
        float[] center = getCenter(bitmap);
        Matrix matrix = new Matrix();
        matrix.postScale(-1, 1, center[0], center[1]);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
    }

    /**
     * Rotate bitmap by degree
     * @param bitmap
     * @param degree
     * @return Bitmap
     */
    public static Bitmap rotateImage(Bitmap bitmap, float degree)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        bitmap.recycle();
        return rotatedBitmap;
    }

    /**
     * Get the center coordinates given a Bitmap.
     *
     * @param bitmap
     * @return float[]
     */
    private float[] getCenter(Bitmap bitmap) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        float centerWidth = width / 2f;
        float centerHeight = height / 2f;
        float[] center = {centerWidth, centerHeight};

        return center;
    }

    /**
     * Get the center coordinates given a Drawable.
     *
     * @param drawable
     * @return float[]
     */
    private float[] getCenter(Drawable drawable) {
        int width = drawable.getIntrinsicWidth();
        int height = drawable.getIntrinsicHeight();
        float centerWidth = width / 2f;
        float centerHeight = height / 2f;
        float[] center = {centerWidth, centerHeight};

        return center;
    }

    private Bitmap drawableToBitmap(Drawable drawable) {
        BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
        return bitmapDrawable.getBitmap();
    }

    private Drawable bitmapToDrawable(Bitmap bitmap) {
        Drawable drawable = new BitmapDrawable(getResources(), bitmap);
        return drawable;
    }

    /**
     * Offsets a foreground image and merges it with a background image.
     *
     * @param background Bitmap
     * @param foreground Bitmap
     * @param x
     * @param y
     * @return Bitmap
     */
    private Bitmap mergeImages(Bitmap background, Bitmap foreground, int x, int y) {
        Bitmap result = Bitmap.createBitmap(background.getWidth(), background.getHeight(), background.getConfig());
        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(background, 0f, 0f, null);
        canvas.drawBitmap(foreground, x, y, null);
        return result;
    }

    /**
     * Sets the paulBitmap to the correct/current properties (direction, size, rotation)
     * to be called before showPaul()
     * Flip, resize, rotate the original image to retain resolution.
     */
    private void drawPaul(){
        Bitmap paulBitmapCopy;

        //get original paul
        paulBitmap = ogPaulBitmap.copy(Bitmap.Config.ARGB_8888, true);

        //flip paul if needed
        if(direction == 0){
            paulBitmapCopy = flipHorizontally(paulBitmap);
            paulBitmap.recycle();
            paulBitmap = paulBitmapCopy;
        }

        //resize paul
        paulBitmapCopy = Bitmap.createScaledBitmap(paulBitmap, (int)(ogPaulBitmap.getWidth() * ratio), (int)(ogPaulBitmap.getHeight() * ratio), false);
        paulBitmap.recycle();
        paulBitmap = paulBitmapCopy;

        //rotate paul
        paulBitmapCopy = rotateImage(paulBitmap, rotation);
        paulBitmap.recycle();
        paulBitmap = paulBitmapCopy;
    }

    /**
     * Displays Paul on the canvas.
     */
    private void showPaul() {
        //Merge Paul with a transparent background to maintain aspect ratio
        //otherwise he will stretch to fill the screen
        paulLayerBitmap = mergeImages(transparentBitmap, paulBitmap, (int) (offsetX), (int) (offsetY));
        canvasBitmap = paulLayerBitmap.copy(Bitmap.Config.ARGB_8888, true);
        canvas.drawBitmap(canvasBitmap, 0, 0, paint);
        paulLayerBitmap.recycle();
        invalidate();
    }



}

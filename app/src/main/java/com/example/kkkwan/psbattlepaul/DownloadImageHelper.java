package com.example.kkkwan.psbattlepaul;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;

/**
 * Created by Kelly on 3/22/2018.
 */

//AsyncTask<Params, Progress, Result>
public class DownloadImageHelper extends AsyncTask<String, Void, Bitmap> {

    private int retrievedImg = 0; //change value depending if there was an error

    @Override
    protected Bitmap doInBackground(String... params) {
        try{
            HttpURLConnection connection = (HttpURLConnection) new URL(params[0]).openConnection();
            connection.connect();
            InputStream input = connection.getInputStream();

            //If the image size is too large, we will get an out of memory error,
            //so only load images of reasonable size
            //It is possible for the content-length header to be incorrect, which will result in OOM and crash
            if(connection.getContentLength() < 550000){
                Bitmap bitmap = BitmapFactory.decodeStream(input);
                retrievedImg = 0;
                input.close();
                return bitmap;
            }
            else{
                input.close();
                retrievedImg = 5;
                return null;
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
            retrievedImg = 1;
            return null;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            retrievedImg = 2;
            return null;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            retrievedImg = 3;
            return null;
        } catch (Exception e) {
            e.printStackTrace();
            retrievedImg = 4;
            return null;
        }
    }

    public int getRetrievedImgBool(){
        return retrievedImg;
    }
}

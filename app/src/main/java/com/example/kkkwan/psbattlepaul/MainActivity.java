package com.example.kkkwan.psbattlepaul;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.File;

public class MainActivity extends AppCompatActivity {

    private CanvasView canvasView;
    private Button galleryButton;
    private Button downloadButton;
    private Button saveButton;
    private Button shareButton;
    private String path;
    private Resources r;
    final int GALLERY_REQUESTCODE = 1;
    private Button toolsButton;
    private ToggleButton flipButton;
    private ToggleButton dragButton;
    private ToggleButton rotateButton;
    private ToggleButton resizeButton;
    private Button exampleButton;

    private enum ToolOption {FLIP, DRAG, ROTATE, RESIZE}

    private ToolOption toolSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        r = getResources();
        galleryButton = (Button) findViewById(R.id.bg_gallery_button);
        downloadButton = (Button) findViewById(R.id.bg_url_button);
        saveButton = (Button) findViewById(R.id.save_button);
        shareButton = (Button) findViewById(R.id.share_button);
        canvasView = (CanvasView) findViewById(R.id.canvasview);
        toolsButton = (Button) findViewById(R.id.tools_button);
        toolSelected = ToolOption.DRAG;

        galleryButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //select photo from gallery; see onActivityResult for details
                Intent selectPhoto = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(selectPhoto, GALLERY_REQUESTCODE);
            }
        });

        downloadButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final EditText editURL = new EditText(MainActivity.this);

                new android.support.v7.app.AlertDialog.Builder(MainActivity.this)
                        .setTitle("Background Image from URL")
                        .setMessage("Paste Full Image URL:")
                        .setView(editURL)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                path = editURL.getText().toString().replace(" ", "").trim();

                                canvasView.setImageFromURL(path);

                                return;

                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                return;
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        saveButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final EditText editTitle = new EditText(MainActivity.this);

                new android.support.v7.app.AlertDialog.Builder(MainActivity.this)
                        .setTitle("Saving Image as JPG")
                        .setMessage("Give the masterpiece a name!")
                        .setView(editTitle)
                        .setPositiveButton("Save", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                String title = editTitle.getText().toString().trim();
                                saveImage(title);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                                return;
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        shareButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                //save image first, then share
                String filePath = saveImage("");
                shareFile(filePath);
            }
        });

        toolsButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final android.support.v7.app.AlertDialog.Builder optionsDialogBuilder = new android.support.v7.app.AlertDialog.Builder(MainActivity.this, R.style.MyDialogTheme);
                final android.support.v7.app.AlertDialog optionsDialog = optionsDialogBuilder.create();
                optionsDialog.setTitle("Options for Paul");
                optionsDialog.setMessage("");

                //custom options in dialog box
                LayoutInflater inflater = getLayoutInflater();
                final View tools_view = inflater.inflate(R.layout.edit_options, null);

                flipButton = (ToggleButton) tools_view.findViewById(R.id.flip_button);
                dragButton = (ToggleButton) tools_view.findViewById(R.id.drag_button);
                rotateButton = (ToggleButton) tools_view.findViewById(R.id.rotate_button);
                resizeButton = (ToggleButton) tools_view.findViewById(R.id.resize_button);
                exampleButton = (Button) tools_view.findViewById(R.id.example_button);

                //set tool after getting the tools view
                setToolOption(toolSelected);

                //whichever button is pressed,
                //remember which tool is being selected
                //tell the canvas view which tool was chosen
                //close the tools menu
                //notify user which tool was selected
                flipButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        toolSelected = ToolOption.FLIP;
                        setToolOption(toolSelected);
                        toastWhichTool(toolSelected);
                        optionsDialog.dismiss();
                    }
                });
                dragButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        toolSelected = ToolOption.DRAG;
                        setToolOption(toolSelected);
                        toastWhichTool(toolSelected);
                        optionsDialog.dismiss();
                    }
                });
                rotateButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        toolSelected = ToolOption.ROTATE;
                        setToolOption(toolSelected);
                        toastWhichTool(toolSelected);
                        optionsDialog.dismiss();
                    }
                });
                resizeButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        toolSelected = ToolOption.RESIZE;
                        setToolOption(toolSelected);
                        toastWhichTool(toolSelected);
                        optionsDialog.dismiss();
                    }
                });
                exampleButton.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {

                        canvasView.showExample();
                        optionsDialog.dismiss();
                        Toast.makeText(MainActivity.this, "An example of what you can do!", Toast.LENGTH_LONG).show();
                    }
                });

                optionsDialog.setView(tools_view);
                optionsDialog.show();
            }
        });

    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUESTCODE && data != null) {
            canvasView.setImageFromGallery(getApplicationContext(), r, data);
        }

    }

    public void toastWhichTool(ToolOption toolSelected){
        String tool;
        switch(toolSelected){
            case FLIP:
                tool = "flip";
                break;
            case DRAG:
                tool = "drag";
                break;
            case ROTATE:
                tool = "rotate";
                break;
            case RESIZE:
                tool = "resize";
                break;
            default:
                tool = "invalid";
                break;
        }
        Toast.makeText(MainActivity.this, String.format("Selected %s tool", tool), Toast.LENGTH_LONG).show();
    }

    /**
     * Turns off all toggle buttons for edit option
     */
    private void resetToolOptions() {
        flipButton.setChecked(false);
        dragButton.setChecked(false);
        rotateButton.setChecked(false);
        resizeButton.setChecked(false);
    }

    /**
     * Shows which tool is currently being used
     */
    private void setToolOption(ToolOption toolSelected) {
        resetToolOptions();

        switch (toolSelected) {
            case FLIP:
                flipButton.setChecked(true);
                canvasView.setTool(CanvasView.ToolOption.FLIP);
                break;
            case DRAG:
                dragButton.setChecked(true);
                canvasView.setTool(CanvasView.ToolOption.DRAG);
                break;
            case ROTATE:
                rotateButton.setChecked(true);
                canvasView.setTool(CanvasView.ToolOption.ROTATE);
                break;
            case RESIZE:
                resizeButton.setChecked(true);
                canvasView.setTool(CanvasView.ToolOption.RESIZE);
                break;
        }
    }

    /**
     * save image and give it a title (file name and path automatically generated)
     *
     * @return String file path
     */
    public String saveImage(String title) {

        if (title.isEmpty()) {
            title = "PSBattle:Paul Image";
        }

        //make background white or else transparencies will save black in jpg
//        layerDrawable.setDrawableByLayerId(0, whiteDrawable);
        canvasView.setTransparentToWhite();

        //attempt saving the image; if it was unsucessful, imageSaved will be null
        canvasView.setDrawingCacheEnabled(true);

//        String imgSaved = MediaStore.Images.Media.insertImage(getContentResolver(), canvasView.getDrawingCache(), title, "image"); //image saves with date 12-31-1969
        String imgSaved = ExternalMethods.insertImage(getContentResolver(), canvasView.getDrawingCache(), title, "image");
        String filePath = ExternalMethods.getRealPathFromUri(getApplicationContext(), Uri.parse(imgSaved));

        //appropriate toast
        if (imgSaved != null) {
            Toast.makeText(MainActivity.this, title + " saved to Gallery!", Toast.LENGTH_LONG).show();
            Toast.makeText(MainActivity.this, title + " was saved at " + filePath, Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(MainActivity.this, "Image could not be saved!", Toast.LENGTH_LONG).show();
        }
        canvasView.destroyDrawingCache();

        return filePath;
    }

    /**
     * Sharing files to whatever app installed can support
     */
    private void shareFile(String filePath) {
        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
        final File imageFile = new File(filePath);
        shareIntent.setType("image/jpg");
        shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(imageFile));
        startActivity(Intent.createChooser(shareIntent, "Share image using"));
    }
}

